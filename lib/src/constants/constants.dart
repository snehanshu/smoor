library constants;

/***      API Links     ***/
const API_LINK = 'https://mocki.io/v1/e7b3766b-3e01-42f6-bd5c-a82bedda41f7';
const API_REALLINK = 'https://dev.iotzen.app/zenPluginManager/shopify/order/orders/17cd02b953a8bf82183245eae3169017/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/smoor-choco/2021-10';
const API_LINK2 = 'https://mocki.io/v1/826b154b-fb0d-4b47-a2f9-3b7b4426ef49';
/***  Images ***/
const Img_DelGuy = 'lib/src/assets/images/delivery_guy_1.png';