import 'package:flutter/material.dart';
import 'package:smoorapplication/src/screen/login/loginpage.dart';

Widget leftDrawer(BuildContext context) {
  return SizedBox(width: 150, child:
  Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: [
        const DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.black,
          ),
          child: Text('Menu', style: TextStyle(color: Colors.white, fontSize: 24),),
        ),
        ListTile(
          title: const Text('Help', style: TextStyle(color: Colors.black, fontSize: 16),),
          onTap: (){
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text('Updates', style: TextStyle(color: Colors.black, fontSize: 16)),
          onTap: (){
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text('Log Out', style: TextStyle(color: Colors.black, fontSize: 16)),
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LoginPage()),
            );
          },
        ),
      ],
    ),
  ),);
}