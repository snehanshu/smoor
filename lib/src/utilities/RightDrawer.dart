import 'package:flutter/material.dart';

Widget rightDrawer(BuildContext context){
  return SizedBox(width: 200, child:
  Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: [
        const DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.black,
          ),
          child: Text('User Profile', style: TextStyle(color: Colors.white, fontSize: 24),),
        ),
        ListTile(
          title: const Text('Orders', style: TextStyle(color: Colors.black, fontSize: 16),),
          onTap: (){
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: const Text('Payments', style: TextStyle(color: Colors.black, fontSize: 16)),
          onTap: (){
            Navigator.pop(context);
          },
        ),
      ],
    ),
  ),);
}