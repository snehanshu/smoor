import 'package:flutter/material.dart';

Widget bottomnavigationbar(){
  return BottomNavigationBar(
    backgroundColor: Colors.black,
    selectedItemColor: Colors.white,
    unselectedItemColor: Colors.grey,
    items: const <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.all_inbox_rounded , color: Colors.white, size: 35),
        label: 'Orders',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.check_circle_outline_rounded, size: 35),
        label: 'Completed',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.poll_outlined, size: 35),
        label: 'Stats',
      ),
    ],
  );
}