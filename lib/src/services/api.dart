import 'dart:convert';
import 'package:http/http.dart';
import 'package:smoorapplication/src/model/class.dart';
import '../constants/constants.dart';

Future<dynamic> apiGetOrder() async{
  var response = await get(Uri.parse(API_LINK2));
  var data = jsonDecode(response.body);
  List<Orders> orders = <Orders>[];
  for(var i in data["orders"]){
    Orders order = Orders.fromJson(i);
    orders.add(order);
  }
  return orders;
}
