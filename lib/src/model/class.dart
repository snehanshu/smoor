class ShopifyObjects {
  late List<Orders> orders;

  ShopifyObjects({required this.orders});

  ShopifyObjects.fromJson(Map<String, dynamic> json) {
    if (json['orders'] != null) {
      orders = <Orders>[];
      json['orders'].forEach((v) {
        orders.add(Orders.fromJson(v));
      });
    }
  }
}

class Orders {
  late int id;
  late List<NoteAttributes> noteAttributes;
  late int number;
  late int orderNumber;
  late List<LineItems> lineItems;
  late ShippingAddress shippingAddress;

  Orders(
      {required this.id,
      required this.noteAttributes,
      required this.number,
      required this.orderNumber,
      required this.lineItems,
      required this.shippingAddress});

  Orders.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['note_attributes'] != null) {
      noteAttributes = <NoteAttributes>[];
      json['note_attributes'].forEach((v) {
        noteAttributes.add(NoteAttributes.fromJson(v));
      });
    }
    number = json['number'];
    orderNumber = json['order_number'];
    if (json['line_items'] != null) {
      lineItems = <LineItems>[];
      json['line_items'].forEach((v) {
        lineItems.add(LineItems.fromJson(v));
      });
    }
    shippingAddress = (json['shipping_address'] != null
        ? ShippingAddress.fromJson(json['shipping_address'])
        : null)!;
  }
}

class NoteAttributes {
  late String name;
  late String value;

  NoteAttributes({required this.name, required this.value});

  NoteAttributes.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }
}

class LineItems {
  late OriginLocation originLocation;

  LineItems({required this.originLocation});

  LineItems.fromJson(Map<String, dynamic> json) {
    originLocation = (json['origin_location'] != null
        ? OriginLocation.fromJson(json['origin_location'])
        : null)!;
  }
}

class OriginLocation {
  late String name;
  late String address1;
  late String address2;
  late String city;
  late String zip;

  OriginLocation(
      {required this.name, required this.address1, required this.address2, required this.city, required this.zip});

  OriginLocation.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    address1 = json['address1'];
    address2 = json['address2'];
    city = json['city'];
    zip = json['zip'];
  }
}

class ShippingAddress {
  late String firstName;
  late String address1;
  late String phone;
  late String city;
  late String zip;
  late String province;
  late String country;
  late String lastName;
  late String address2;
  late String company;
  late double latitude;
  late double longitude;
  late String name;
  late String countryCode;
  late String provinceCode;

  ShippingAddress(
      {required this.firstName,
      required this.address1,
      required this.phone,
      required this.city,
      required this.zip,
      required this.province,
      required this.country,
      required this.lastName,
      required this.address2,
      required this.company,
      required this.latitude,
      required this.longitude,
      required this.name,
      required this.countryCode,
      required this.provinceCode});

  ShippingAddress.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    address1 = json['address1'];
    phone = json['phone'];
    city = json['city'];
    zip = json['zip'];
    province = json['province'];
    country = json['country'];
    lastName = json['last_name'];
    address2 = json['address2'];
    company = json['company'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    name = json['name'];
    countryCode = json['country_code'];
    provinceCode = json['province_code'];
  }
}
