import 'package:flutter/material.dart';
import 'package:smoorapplication/src/screen/home/homepage.dart';
import '../../constants/constants.dart';

class BodyFunctionLP extends StatefulWidget{
  const BodyFunctionLP({Key? key}) : super(key: key);

  @override
  _BodyFunctionLPState createState() => _BodyFunctionLPState();
}

class _BodyFunctionLPState extends State<BodyFunctionLP>{
  @override
  Widget build(BuildContext context){
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 250,
                width: 330,
                margin: const EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border:
                  Border.all(
                    width: 2,
                    color: Colors.grey,
                  ),
                ),
                child:  Column(
                  children: [
                    const SizedBox(height: 30,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text('Enter mobile number: ', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 200,
                          child: Column(
                            children: const [
                              TextField(
                                keyboardType: TextInputType.number,
                                obscureText: true,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: '10 Digits',
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          child: Container(
                            height: 50,
                            width: 100,
                            margin: const EdgeInsets.all(0.0),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              border:
                              Border.all(
                                width: 1,
                                //color: Colors.grey,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [
                                Text('LOGIN', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),)
                              ],
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => const HomePage()),
                              );
                            });
                          },
                        )
                      ],
                    )
                  ],
                ),
              )
            ],),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(Img_DelGuy, height: 200,)
            ],
          ),
        ],
      ),
    );
  }
}