import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:smoorapplication/src/screen/home/bodyfncard.dart';

int timeLeft = 50;

Widget boxedOrder(
    bool isExpanded,
    bool isAccepted,
    bool isPicked,
    bool isDelivered,
    bool isRetDel,
    bool isArrived,
    bool isReturned,
    bool isFinished,
    bool isNotAccepted,
    bool showCon,
    int orderNumber,
    double progress,
    String shopName,
    String pl1,
    String pl2,
    String dl1_1,
    String dl1_2,
    String dl2,
    String orderStatus,
    String cusName,
    String cusPhone,
    CallService _service,
    ){
   return
   isExpanded ?
   /*** isExpanded condition is true! ***/
     Column(
     children: <Widget>[
       const SizedBox(height: 10,),
       InkWell(child:
         Container(
           height: 345,
           width: 360,
           decoration: BoxDecoration(
             border: Border.all(width: 3.0, color: isAccepted ? Colors.green : Colors.grey),
           ),
           child: Column(children: <Widget>[
             Container(
               height: 30,
               width: 360,
               color: const Color.fromRGBO(220,220,220,2.0),
               child: Row(children: <Widget>[
                 Text('  $orderNumber', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                 const SizedBox(width: 90,),
                 LinearPercentIndicator(
                   width: 130,
                   lineHeight: 7,
                   percent: progress,
                   backgroundColor: const Color.fromRGBO(255, 102, 0, 2.0),
                   progressColor: const Color.fromRGBO(0, 153, 51, 1.0),
                 ),
                 const SizedBox(width: 10,),
                 Text('  $timeLeft  Mins', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)
               ],),
             ),
             const SizedBox(height: 10,),
             Row(children: <Widget>[
               RichText(text: const TextSpan(
                 children: [
                   WidgetSpan(
                     child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.circle, color: Color.fromRGBO(255, 102, 0, 2.0), size: 18,),)
                   )
                 ]
               )),
               const Text('PICKUP LOCATION', style: TextStyle(fontSize: 16),)
             ],),
             const SizedBox(height: 5,),
             Row(children: <Widget>[
               Text('        $shopName $pl1',
                 style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
               )
             ],),
             Row(children: <Widget>[
               Text('        $dl2',
                 style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
               )
             ],),
             const SizedBox(height: 10,),
             Row(children: <Widget>[
               RichText(text: const TextSpan(
                   children: [
                     WidgetSpan(
                         child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.circle, color: Color.fromRGBO(0, 153, 51, 1.0), size: 18,),)
                     )
                   ]
               )),
               const Text('DELIVERY LOCATION', style: TextStyle(fontSize: 16),)
             ],),
             const SizedBox(height: 5,),
             Row(children: <Widget>[
               Text('        $dl1_1',
                 style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
               )
             ],),
             Row(children: <Widget>[
               Text('        $dl1_2 $dl2',
                 style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
               )
             ],),
             const SizedBox(height: 10,),
             Row(children: <Widget>[
               RichText(text: const TextSpan(
                   children: [
                     WidgetSpan(
                         child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.arrow_forward_ios_sharp, color: Colors.red, size: 18,),)
                     )
                   ]
               )),
               Text('Order status: $orderStatus', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)
             ],),
             const SizedBox(height: 10,),
             if(isRetDel) Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: <Widget>[
                 InkWell(child: Container(height: 50, width: 200, color: Colors.black,child:
                 Row(mainAxisAlignment: MainAxisAlignment.center,
                   children: <Widget>[Text(isFinished ? 'FINISHED' : 'FINISH', style: const TextStyle(fontSize: 16, color:
                   Colors.white),)],),),onTap: (){
                   isFinished? null :
                   isFinished = !isFinished;
                   orderStatus = 'Finished';
                   progress += 0.2;},)
               ],),
             if(isArrived) Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: <Widget>[
                 InkWell(child: Container(height: 50, width: 150, color: Colors.black,child:
                 Row(mainAxisAlignment: MainAxisAlignment.center,
                   children: const <Widget>[Text('DELIVERED', style: TextStyle(fontSize: 16, color:
                   Colors.white),)],),),onTap: (){
                     isDelivered = !isDelivered;
                     isRetDel = !isRetDel;
                     isArrived = !isArrived;
                     orderStatus = 'Delivered';
                     progress += 0.2;},),
                 const SizedBox(width: 10,),
                 Row(
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: <Widget>[
                     InkWell(child: Container(height: 50, width: 150, color: Colors.black,child:
                     Row(mainAxisAlignment: MainAxisAlignment.center,
                       children: const <Widget>[Text('RETURNED', style: TextStyle(fontSize: 16, color:
                       Colors.white),)],),),onTap: (){
                         isReturned = !isReturned;
                         isRetDel = !isRetDel;
                         isArrived = !isArrived;
                         orderStatus = 'Returned';
                         progress += 0.2;},)
                   ],),
               ],),
             if(isPicked) Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: <Widget>[
                 InkWell(child: Container(height: 50, width: 200, color: Colors.black,child:
                 Row(mainAxisAlignment: MainAxisAlignment.center,
                   children: const <Widget>[Text('ARRIVED', style: TextStyle(fontSize: 16, color:
                   Colors.white),)],),),onTap: (){
                     isArrived = !isArrived;
                     isPicked = !isPicked;
                     orderStatus = 'Arrived';
                     progress += 0.2;},)
               ],),
             if(isAccepted) Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: <Widget>[
                 InkWell(child: Container(height: 50, width: 200, color: Colors.black,child:
                 Row(mainAxisAlignment: MainAxisAlignment.center,
                   children: const <Widget>[Text('PICKED', style: TextStyle(fontSize: 16, color:
                   Colors.white),)],),),onTap: (){
                     isAccepted = !isAccepted;
                     isPicked = !isPicked;
                     orderStatus = 'Picked';
                     progress += 0.2;},)
               ],),
             if(!isNotAccepted) Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: <Widget>[
                 InkWell(child: Container(height: 50, width: 200, color: Colors.black,child:
                 Row(mainAxisAlignment: MainAxisAlignment.center,
                   children: const <Widget>[Text('ACCEPT', style: TextStyle(fontSize: 16, color:
                   Colors.white),)],),),onTap: (){
                     isNotAccepted = !isNotAccepted;
                     isAccepted = !isAccepted;
                     showCon = !showCon;
                     orderStatus = 'Accepted';
                     progress += 0.2;},)
               ],),
             const SizedBox(height: 10,),
             Container(height: 55, width: 330, decoration:
             BoxDecoration(color: const Color.fromRGBO(220,220,220,2.0), borderRadius: BorderRadius.circular(10.0)),
             child:
             showCon ?
             Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
               Column(children: <Widget>[
                 Text(cusName, style: const TextStyle(fontSize: 18),),
                 const SizedBox(height: 10,),
                 Text(cusPhone, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
               ],),
               const SizedBox(width: 20,),
               ElevatedButton(onPressed: (){
                 _service.call(cusPhone);
               }, child: const Icon(Icons.call, color: Colors.white,))
             ],) : Row(mainAxisAlignment: MainAxisAlignment.center,
                 children: const <Widget>[
                   Text('Order is not yet accepted!', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)]),
             )
     ],),),
       onTap: (){
         isExpanded = !isExpanded;},
       ),],)
       /*** isExpanded condition is false! ***/
       : Column(
     children: <Widget>[
       const SizedBox(height: 10,),
       InkWell(child:
       Container(
         height: 220,
         width: 360,
         margin: const EdgeInsets.all(0.0),
         decoration: BoxDecoration(
           border: Border.all(width: 3.0, color: isAccepted ? Colors.green : Colors.grey),
         ),
         child: Column(children: <Widget>[
           Container(
             height: 30,
             width: 360,
             color: const Color.fromRGBO(220,220,220,2.0),
             child: Row(children: <Widget>[
               Text('  $orderNumber', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
               const SizedBox(width: 90,),
               LinearPercentIndicator(
                 width: 130,
                 lineHeight: 7,
                 percent: progress,
                 backgroundColor: const Color.fromRGBO(255, 102, 0, 2.0),
                 progressColor: const Color.fromRGBO(0, 153, 51, 1.0),
               ),
               const SizedBox(width: 10,),
               Text('  $timeLeft Mins', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)
             ],),
           ),
           const SizedBox(height: 10,),
           Row(children: <Widget>[
             RichText(text: const TextSpan(
                 children: [
                   WidgetSpan(
                       child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.circle, color: Color.fromRGBO(255, 102, 0, 2.0), size: 18,),)
                   )
                 ]
             )),
             const Text('PICKUP LOCATION', style: TextStyle(fontSize: 16),)
           ],),
           const SizedBox(height: 5,),
           Row(children: <Widget>[
             Text('        $shopName $pl1',
               style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
             )
           ],),
           Row(children: <Widget>[
             Text('        $pl2',
               style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
             )
           ],),
           const SizedBox(height: 10,),
           Row(children: <Widget>[
             RichText(text: const TextSpan(
                 children: [
                   WidgetSpan(
                       child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.circle, color: Color.fromRGBO(0, 153, 51, 1.0), size: 18,),)
                   )
                 ]
             )),
             const Text('DELIVERY LOCATION', style: TextStyle(fontSize: 16),)
           ],),
           const SizedBox(height: 5,),
           Row(children: <Widget>[
             Text('        $dl1_1',
               style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
             )
           ],),
           Row(children: <Widget>[
             Text('        $dl1_2 $dl2',
               style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
             )
           ],),
           const SizedBox(height: 10,),
           Row(children: <Widget>[
             RichText(text: const TextSpan(
                 children: [
                   WidgetSpan(
                       child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.arrow_forward_ios_sharp, color: Colors.red, size: 18,),)
                   )
                 ]
             )),
             Text('Order status: $orderStatus', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)
           ],)
         ],),
       ),
         onTap: (){
             //isExpanded = !isExpanded;
             // print(isExpanded);
            //boxedOrder(orderNumber,shopName,pl1,pl2,dl1_1,dl1_2,dl2,cusName,cusPhone,_service);
         },
       ),
     ],
   );
}

/***

    snapshot.data[index].orderNumber
    snapshot.data[index].lineItems[0].originLocation.name
    snapshot.data[index].lineItems[0].originLocation.address1
    snapshot.data[index].lineItems[0].originLocation.address2
    snapshot.data[index].shippingAddress.address1.substring(0,37)
    snapshot.data[index].shippingAddress.address1.substring(39)
    snapshot.data[index].shippingAddress.address2
    snapshot.data[index].shippingAddress.name
    cusPhone

***/