import 'package:flutter/material.dart';
import 'package:smoorapplication/src/screen/home/bodyfncard.dart';
import 'package:smoorapplication/src/utilities/navigationbar/BottomNavigationBar.dart';
import 'package:smoorapplication/src/utilities/drawer/LeftDrawer.dart';
import 'package:smoorapplication/src/utilities/drawer/RightDrawer.dart';

class HomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'SMOOR',
      home: _HomePageState(),
    );
  }
}

class _HomePageState extends StatelessWidget{
  var scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: true,
        appBar:  AppBar(
            toolbarHeight: 80,
            backgroundColor: Colors.black,
            centerTitle: true,
            title: Image.asset('lib/src/assets/images/smoorlogo.png', height: 55),
            leading: IconButton(
              icon: const Icon(Icons.dehaze_sharp),
              iconSize: 40,
              onPressed: () => scaffoldKey.currentState!.openDrawer(),
            ),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.person_sharp),
                iconSize: 40,
                onPressed: () => scaffoldKey.currentState!.openEndDrawer(),
              ),
            ]
        ),
      body: const BodyFunctionCard(),
      drawer: leftDrawer(context),
      endDrawer: rightDrawer(context),
      bottomNavigationBar: bottomnavigationbar(),
    );
  }
}