import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:smoorapplication/src/services/api.dart';
import 'package:url_launcher/url_launcher.dart';

class CallService{
  void call(String number) => launch("tel:$number");
}

void set(){
  locator.registerSingleton(CallService());
}

GetIt locator = GetIt.asNewInstance();

class BodyFunctionCard extends StatefulWidget{
  const BodyFunctionCard({Key? key}) : super(key: key);
  @override
  _BodyFunctionCardHPState createState() => _BodyFunctionCardHPState();
}

class _BodyFunctionCardHPState extends State<BodyFunctionCard>{
  List<bool> isExpanded = List.generate(100, (index) => false);
  //bool isExpanded = true;
  List<bool> isAccepted = List.generate(100,(index) => false);
  List<bool> isPicked = List.generate(100,(index) => false);
  List<bool> isArrived = List.generate(100,(index) => false);
  List<bool> isRetDel = List.generate(100,(index) => false);
  List<bool> isReturned = List.generate(100,(index) => false);
  List<bool> isDelivered = List.generate(100,(index) => false);
  List<bool> isFinished = List.generate(100,(index) => false);
  List<bool> isNotAccepted = List.generate(100,(index) => false);
  List<bool> countfin = List.generate(100,(index) => false);
  List<bool> showCon = List.generate(100,(index) => false);
  List<double> progress = List.generate(100, (index) => 0.0);
  List<String> orderStatus = List.generate(100, (index) => 'Not Accepted');
  final CallService _service = locator<CallService>();
  @override
  Widget build(BuildContext context){
    return Card(
        child: FutureBuilder(
          future: apiGetOrder(),
          builder: (BuildContext context, AsyncSnapshot snapshot){
              if(snapshot.connectionState == ConnectionState.none){
                return const Center(child:
                Text('Loading data . . . !'),);
              }return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                  return
                  //   InkWell(child:
                  //   boxedOrder(
                  //   isExpanded[index],
                  //   isAccepted[index],
                  //   isPicked[index],
                  //   isDelivered[index],
                  //   isRetDel[index],
                  //   isArrived[index],
                  //   isReturned[index],
                  //   isFinished[index],
                  //   isNotAccepted[index],
                  //   showCon[index],
                  //   snapshot.data[index].orderNumber,
                  //   progress[index],
                  //   snapshot.data[index].lineItems[0].originLocation.name,
                  //   snapshot.data[index].lineItems[0].originLocation.address1,
                  //   snapshot.data[index].lineItems[0].originLocation.address2,
                  //   snapshot.data[index].shippingAddress.address1.substring(0,37),
                  //   snapshot.data[index].shippingAddress.address1.substring(39),
                  //   snapshot.data[index].shippingAddress.address2,
                  //   orderStatus[index],
                  //   snapshot.data[index].shippingAddress.name,
                  //   snapshot.data[index].shippingAddress.phone,
                  //     _service
                  // ),onTap: (){
                  //     isExpanded[index] = !isExpanded[index];
                  // },);


                    isExpanded[index] && !isFinished[index] ?
                /*** isExpanded condition is true! ***/
                  Column(
                  children: <Widget>[
                    const SizedBox(height: 10,),
                    InkWell(child:
                      Container(
                        height: 345,
                        width: 360,
                        decoration: BoxDecoration(
                          border: Border.all(width: 3.0, color: isAccepted[index] ? Colors.green : Colors.grey),
                        ),
                        child: Column(children: <Widget>[
                          Container(
                            height: 30,
                            width: 360,
                            color: const Color.fromRGBO(220,220,220,2.0),
                            child: Row(children: <Widget>[
                              Text('  ${snapshot.data[index].orderNumber}', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                              const SizedBox(width: 90,),
                              LinearPercentIndicator(
                                width: 130,
                                lineHeight: 7,
                                percent: progress[index],
                                backgroundColor: const Color.fromRGBO(255, 102, 0, 2.0),
                                progressColor: const Color.fromRGBO(0, 153, 51, 1.0),
                              ),
                              const SizedBox(width: 10,),
                              Text('  ${snapshot.data[index].orderNumber}  ', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)
                            ],),
                          ),
                          const SizedBox(height: 10,),
                          Row(children: <Widget>[
                            RichText(text: const TextSpan(
                              children: [
                                WidgetSpan(
                                  child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.circle, color: Color.fromRGBO(255, 102, 0, 2.0), size: 18,),)
                                )
                              ]
                            )),
                            const Text('PICKUP LOCATION', style: TextStyle(fontSize: 16),)
                          ],),
                          const SizedBox(height: 5,),
                          Row(children: <Widget>[
                            Text('        ${snapshot.data[index].lineItems[0].originLocation.name} ${snapshot.data[index].lineItems[0].originLocation.address1}',
                              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                            )
                          ],),
                          Row(children: <Widget>[
                            Text('        ${snapshot.data[index].lineItems[0].originLocation.address2}',
                              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                            )
                          ],),
                          const SizedBox(height: 10,),
                          Row(children: <Widget>[
                            RichText(text: const TextSpan(
                                children: [
                                  WidgetSpan(
                                      child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.circle, color: Color.fromRGBO(0, 153, 51, 1.0), size: 18,),)
                                  )
                                ]
                            )),
                            const Text('DELIVERY LOCATION', style: TextStyle(fontSize: 16),)
                          ],),
                          const SizedBox(height: 5,),
                          Row(children: <Widget>[
                            Text('        ${snapshot.data[index].shippingAddress.address1.substring(0,37)}',
                              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                            )
                          ],),
                          Row(children: <Widget>[
                            Text('        ${snapshot.data[index].shippingAddress.address1.substring(39)} ${snapshot.data[index].shippingAddress.address2}',
                              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                            )
                          ],),
                          const SizedBox(height: 10,),
                          Row(children: <Widget>[
                            RichText(text: const TextSpan(
                                children: [
                                  WidgetSpan(
                                      child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.arrow_forward_ios_sharp, color: Colors.red, size: 18,),)
                                  )
                                ]
                            )),
                            Text('Order status: ${orderStatus[index]}', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                            const SizedBox(width: 10,),
                            isFinished[index] ? const Icon(Icons.done_all_rounded, color: Colors.green,) : const Icon(Icons.done_all_rounded, color: Colors.white,)
                          ],),
                          const SizedBox(height: 10,),
                          if(isRetDel[index]) Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              InkWell(child: Container(height: 50, width: 200, color: Colors.black,child:
                              Row(mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[Text(isFinished[index] ? 'FINISHED' : 'FINISH', style: const TextStyle(fontSize: 16, color:
                                Colors.white),)],),),onTap: (){
                                isFinished[index]? null :
                                setState(() {
                                isFinished[index] = !isFinished[index];
                                orderStatus[index] = 'Finished';
                                progress[index] += 0.2;
                              });},)
                            ],),
                          if(isArrived[index]) Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              InkWell(child: Container(height: 50, width: 150, color: Colors.black,child:
                              Row(mainAxisAlignment: MainAxisAlignment.center,
                                children: const <Widget>[Text('DELIVERED', style: TextStyle(fontSize: 16, color:
                                Colors.white),)],),),onTap: (){
                                setState(() {
                                  isDelivered[index] = !isDelivered[index];
                                  isRetDel[index] = !isRetDel[index];
                                  isArrived[index] = !isArrived[index];
                                  orderStatus[index] = 'Delivered';
                                  progress[index] += 0.2;
                                });},),
                              const SizedBox(width: 10,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  InkWell(child: Container(height: 50, width: 150, color: Colors.black,child:
                                  Row(mainAxisAlignment: MainAxisAlignment.center,
                                    children: const <Widget>[Text('RETURNED', style: TextStyle(fontSize: 16, color:
                                    Colors.white),)],),),onTap: (){
                                    setState(() {
                                      isReturned[index] = !isReturned[index];
                                      isRetDel[index] = !isRetDel[index];
                                      isArrived[index] = !isArrived[index];
                                      orderStatus[index] = 'Returned';
                                      progress[index] += 0.2;
                                    });},)
                                ],),
                            ],),
                          if(isPicked[index]) Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              InkWell(child: Container(height: 50, width: 200, color: Colors.black,child:
                              Row(mainAxisAlignment: MainAxisAlignment.center,
                                children: const <Widget>[Text('ARRIVED', style: TextStyle(fontSize: 16, color:
                                Colors.white),)],),),onTap: (){
                                setState(() {
                                  isArrived[index] = !isArrived[index];
                                  isPicked[index] = !isPicked[index];
                                  orderStatus[index] = 'Arrived';
                                  progress[index] += 0.2;
                                });},)
                            ],),
                          if(isAccepted[index]) Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              InkWell(child: Container(height: 50, width: 200, color: Colors.black,child:
                              Row(mainAxisAlignment: MainAxisAlignment.center,
                                children: const <Widget>[Text('PICKED', style: TextStyle(fontSize: 16, color:
                                Colors.white),)],),),onTap: (){
                                setState(() {
                                  isAccepted[index] = !isAccepted[index];
                                  isPicked[index] = !isPicked[index];
                                  orderStatus[index] = 'Picked';
                                  progress[index] += 0.2;
                                });},)
                            ],),
                          if(!isNotAccepted[index]) Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              InkWell(child: Container(height: 50, width: 200, color: Colors.black,child:
                              Row(mainAxisAlignment: MainAxisAlignment.center,
                                children: const <Widget>[Text('ACCEPT', style: TextStyle(fontSize: 16, color:
                                Colors.white),)],),),onTap: (){
                                setState(() {
                                  isNotAccepted[index] = !isNotAccepted[index];
                                  isAccepted[index] = !isAccepted[index];
                                  showCon[index] = !showCon[index];
                                  orderStatus[index] = 'Accepted';
                                  progress[index] += 0.2;
                                });},)
                            ],),
                          const SizedBox(height: 10,),
                          Container(height: 55, width: 330, decoration:
                          BoxDecoration(color: const Color.fromRGBO(220,220,220,2.0), borderRadius: BorderRadius.circular(10.0)),
                          child:
                          showCon[index] ?
                          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                            Column(children: <Widget>[
                              Text('${snapshot.data[index].shippingAddress.name}', style: const TextStyle(fontSize: 18),),
                              const SizedBox(height: 10,),
                              Text('${snapshot.data[index].shippingAddress.phone}', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                            ],),
                            const SizedBox(width: 20,),
                            ElevatedButton(onPressed: (){
                              _service.call(snapshot.data[index].shippingAddress.phone);
                            }, child: const Icon(Icons.call, color: Colors.white,))
                          ],) : Row(mainAxisAlignment: MainAxisAlignment.center,
                              children: const <Widget>[
                                Text('Order is not yet accepted!', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)]),
                          )
                  ],),),
                    onTap: (){setState(() {
                      isExpanded[index] = !isExpanded[index];
                    },);},
                      onLongPress: (){
                      isFinished[index] ?
                        setState(() {
                          snapshot.data.removeAt(index);
                          isExpanded[index] = false;
                          isAccepted[index] = false;
                          isPicked[index] = false;
                          isArrived[index] = false;
                          isRetDel[index] = false;
                          isReturned[index] = false;
                          isDelivered[index] = false;
                          isFinished[index] = false;
                          isNotAccepted[index] = false;
                          countfin[index] = false;
                          progress[index] = 0.0;
                          orderStatus[index] = 'Not Accepted';
                        }) : null;
                      },
                    ),],)
                    /*** isExpanded condition is false! ***/
                    : Column(
                  children: <Widget>[
                    const SizedBox(height: 10,),
                    InkWell(child:
                    Container(
                      height: 220,
                      width: 360,
                      margin: const EdgeInsets.all(0.0),
                      decoration: BoxDecoration(
                        border: Border.all(width: 3.0, color: isAccepted[index] ? Colors.green : Colors.grey),
                      ),
                      child: Column(children: <Widget>[
                        Container(
                          height: 30,
                          width: 360,
                          color: const Color.fromRGBO(220,220,220,2.0),
                          child: Row(children: <Widget>[
                            Text('  ${snapshot.data[index].orderNumber}', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                            const SizedBox(width: 90,),
                            LinearPercentIndicator(
                              width: 130,
                              lineHeight: 7,
                              percent: progress[index],
                              backgroundColor: const Color.fromRGBO(255, 102, 0, 2.0),
                              progressColor: const Color.fromRGBO(0, 153, 51, 1.0),
                            ),
                            const SizedBox(width: 10,),
                            Text('  ${snapshot.data[index].orderNumber}', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)
                          ],),
                        ),
                        const SizedBox(height: 10,),
                        Row(children: <Widget>[
                          RichText(text: const TextSpan(
                              children: [
                                WidgetSpan(
                                    child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.circle, color: Color.fromRGBO(255, 102, 0, 2.0), size: 18,),)
                                )
                              ]
                          )),
                          const Text('PICKUP LOCATION', style: TextStyle(fontSize: 16),)
                        ],),
                        const SizedBox(height: 5,),
                        Row(children: <Widget>[
                          Text('        ${snapshot.data[index].lineItems[0].originLocation.name} ${snapshot.data[index].lineItems[0].originLocation.address1}',
                            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          )
                        ],),
                        Row(children: <Widget>[
                          Text('        ${snapshot.data[index].lineItems[0].originLocation.address2}',
                            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          )
                        ],),
                        const SizedBox(height: 10,),
                        Row(children: <Widget>[
                          RichText(text: const TextSpan(
                              children: [
                                WidgetSpan(
                                    child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.circle, color: Color.fromRGBO(0, 153, 51, 1.0), size: 18,),)
                                )
                              ]
                          )),
                          const Text('DELIVERY LOCATION', style: TextStyle(fontSize: 16),)
                        ],),
                        const SizedBox(height: 5,),
                        Row(children: <Widget>[
                          Text('        ${snapshot.data[index].shippingAddress.address1.substring(0,37)}',
                            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          )
                        ],),
                        Row(children: <Widget>[
                          Text('        ${snapshot.data[index].shippingAddress.address1.substring(39)} ${snapshot.data[index].shippingAddress.address2}',
                            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                          )
                        ],),
                        const SizedBox(height: 10,),
                        Row(children: <Widget>[
                          RichText(text: const TextSpan(
                              children: [
                                WidgetSpan(
                                    child: Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),child: Icon(Icons.arrow_forward_ios_sharp, color: Colors.red, size: 18,),)
                                )
                              ]
                          )),
                          Text('Order status: ${orderStatus[index]}', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                          const SizedBox(width: 10,),
                          isFinished[index] ? const Icon(Icons.done_all_rounded, color: Colors.green,) : const Icon(Icons.done_all_rounded, color: Colors.white,)
                        ],)
                      ],),
                    ),
                      onTap: (){
                        setState(() {
                          isExpanded[index] = !isExpanded[index];
                        });
                      },
                      onLongPress: (){
                      isFinished[index] ?
                        setState(() {
                          snapshot.data.removeAt(index);
                          isExpanded[index] = false;
                          isAccepted[index] = false;
                          isPicked[index] = false;
                          isArrived[index] = false;
                          isRetDel[index] = false;
                          isReturned[index] = false;
                          isDelivered[index] = false;
                          isFinished[index] = false;
                          isNotAccepted[index] = false;
                          countfin[index] = false;
                          progress[index] = 0.0;
                          orderStatus[index] = 'Not Accepted';
                        }) : null;
                      },
                    ),
                  ],
                );
            }
          );
        }
      ),
    );
  }
}